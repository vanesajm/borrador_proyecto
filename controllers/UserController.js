const requestJson = require('request-json');
const io = require ('../io');
const crypt = require ('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuvjm11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");

//  res.sendFile('usuarios.json', {root: __dirname});
// __dirname indica el directorio donde se esta ejecutando el scripts


console.log ("Query strings");
console.log (req.query);



// es igual q lo anterior pero manejando una variable en memoria
var users = require ('../usuarios.json');

//if (req.query.$count)


//res.send(users);

var result = {};


  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0,req.query.$top) : users;

    res.send(result);

//  if (req.query.$top){
//      res.send(users.slice(0,req.query.$top));
//    }

//  else
//    {      res.send(users);
//    }

//    if (req.query.$count){
//        res.send(users.slice(0,req.query.$top),users.length);
//      }

//    else
//      {
//        res.send(users,users.length);
//      }


}

function getUsersV2 (req, res) {
  console.log("GET /apitechu/v2/users");
  var httpCllient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
  //montar la URL que queremos mandar
  httpCllient.get("user?" + mLabAPIKey,
    function (err, resMLab, body){
    var response = !err ? body : {
      "msg" : "Error obteniendo usuarios"
    }
    res.send(response);
    }
  )
}

function getUsersByIdV2 (req, res) {
  console.log("GET /apitechu/v2/users/:id");
  var httpCllient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado byID");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  console.log("La consulta es " + query);

  /*httpCllient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
    var response = !err ? body [0] : {
      "msg" : "Error obteniendo usuario"
    }
    res.send(response);
  }*/
  //montar la URL que queremos mandar
  httpCllient.get("user?" + query + "&" + mLabAPIKey,
    function (err, resMLab, body){
      if (err){
          var response =  {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if(body.length > 0){
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
                }
            res.status(404);
           }
       }
  res.send(response);
}
  )
}

function createUserV1 (req, res) {
  console.log("POST /apitechu/v1/users");

  //console.log(req.headers);
  //console.log(req.headers.first_name);
  //console.log(req.headers.last_name);
  //console.log(req.headers.email);

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  //var newUser = {
  //  "first_name" : req.headers.first_name,
  //  "last_name" : req.headers.last_name,
  //  "email" : req.headers.email
  //};

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users);

  console.log("Proceso creacion usuario terminado");

  res.send({"msg": "usuario creado"});

}

function createUserV2 (req, res) {
  console.log("POST /apitechu/v2/users");
  console.log("id es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("Password es  " + req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  }
  console.log(newUser);

  httpCllient = requestJson.createClient(baseMLabURL);
  httpCllient.post("user?" + mLabAPIKey, newUser,
    function(err,resMLab,body){
      console.log("Usuario guardado");
      res.status(201).send ({"msg" : "Usuario creado"});
  }
)
}

function deleteUsersV1 (req, res) {
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("Id usuario a borrar es " + req.params.id);

  var users = require('../usuarios.json');

  var deleted = false;

 console.log("Usando for normal");
 for (var i = 0; i < users.length; i++) {
   console.log("comparando " + users[i].id + " y " +  req.params.id);
   if (users[i].id == req.params.id) {
     console.log("La posicion " + i + " coincide");
     users.splice(i, 1);
     deleted = true;
     break;
   }
}


// console.log("Usando for in");
// for (arrayId in users) {
//   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
//   if (users[arrayId].id == req.params.id) {
//     console.log("La posicion " + arrayId " coincide");
//     users.splice(arrayId, 1);
//     deleted = true;
//     break;
//   }
// }


// console.log("Usando for of");
// for (user of users) {
//   console.log("comparando " + user.id + " y " +  req.params.id);
//   if (user.id == req.params.id) {
//     console.log("La posición ? coincide");
//     // Which one to delete? order is not guaranteed...
//     deleted = false;
//     break;
//   }
// }

// console.log("Usando for of 2");
// // Destructuring, nodeJS v6+
// for (var [index, user] of users.entries()) {
//   console.log("comparando " + user.id + " y " +  req.params.id);
//   if (user.id == req.params.id) {
//     console.log("La posicion " + index + " coincide");
//     users.splice(index, 1);
//     deleted = true;
//     break;
//   }
// }

// console.log("Usando for of 3");
// var index = 0;
// for (user of users) {
//   console.log("comparando " + user.id + " y " +  req.params.id);
//   if (user.id == req.params.id) {
//     console.log("La posición " + index + " coincide");
//     users.splice(index, 1);
//     deleted = true;
//     break;
//   }
//   index++;
// }


// console.log("Usando Array ForEach");
// users.forEach(function (user, index) {
//   console.log("comparando " + user.id + " y " +  req.params.id);
//   if (user.id == req.params.id) {
//     console.log("La posicion " + index + " coincide");
//     users.splice(index, 1);
//     deleted = true;
//   }
// });

// console.log("Usando Array findIndex");
// var indexOfElement = users.findIndex(
//   function(element){
//     console.log("comparando " + element.id + " y " +   req.params.id);
//     return element.id == req.params.id
//   }
// )
//
// console.log("indexOfElement es " + indexOfElement);
// if (indexOfElement >= 0) {
//   users.splice(indexOfElement, 1);
//   deleted = true;
// }
//
 /*if (deleted) {
   io.writeUserDataToFile(users);
 }

 var msg = deleted ?
 "Usuario borrado" : "Usuario no encontrado."

 console.log(msg);
 res.send({"msg" : msg});


//  users.splice(req.params.id - 1, 1);
//  console.log("Usuario borrado");
//  writeUserDataToFile(users);


//  console.log("Proceso borrado usuario terminado");
//  res.send({"msg" : "usuario borrado"});
*/
  }

  function deleteUsersV2 (req, res) {
    console.log("DELETE /apitechu/v2/users/:id");
    var httpCllient = requestJson.createClient(baseMLabURL);

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    console.log("La consulta es " + query);
    var putBody=[];
    httpCllient.put("user?" + query + "&" + mLabAPIKey, putBody,
    function (err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error borando usuario"
        }
        res.status(500);
      } else {
        var response = {
          "msg" : "Usuario borrado " + id
        }
      }
      res.send(response);
    }
    )
  }

module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.getUsersV2 = getUsersV2;
module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.deleteUsersV2 = deleteUsersV2;
module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV2 = createUserV2;
module.exports.createUserV1 = createUserV1;
