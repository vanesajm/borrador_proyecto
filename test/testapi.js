//www.chaijs.com/api/bdd

const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();
/*
const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();

//generar suite de test
describe("First test",
  function(){
      it("Test that DuckDuckGo works", function(done){
        //chai.request('http://www.duckduckgo.com')
        chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
              function(err, res){
                  console.log("Reques finished");
                  console.log(res);
                  console.log(err);
                  res.should.have.status(200);
                  done(); //estamos en un contexto asincrono. el framework no sabe cuando se acaba
              }
          )
      }
    )
  }
)*/

//generar suite de test
describe("Test de Api Usuarios",
  function(){
      it("Prueba que la API responde", function(done){
        //chai.request('http://www.duckduckgo.com')
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
              function(err, res){
                  console.log("Reques finished");
                  res.should.have.status(200);
                  res.body.msg.should.be.eql("Hola desde APITechu");
                  done(); //estamos en un contexto asincrono. el framework no sabe cuando se acaba
              }
          )
      }
    ),

    it("Prueba que la API devuelve una lista de usuarios correctos", function(done){
      //chai.request('http://www.duckduckgo.com')
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
            function(err, res){
                console.log("Reques finished");
                res.should.have.status(200);
                res.body.users.should.be.a("array");
                for (user of res.body.users){
                    user.should.have.property('id');
                    user.should.have.property('email');
                }
                done(); //estamos en un contexto asincrono. el framework no sabe cuando se acaba
            }
        )
    }
  )
  }
)
