import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        .redbg{
          background-color:red;
        }

        .bluebg{
          background-color:blue;
        }

        .greybg{
          background-color:grey;
        }

        .greenbg{
          background-color:green;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h3>y mi email es  [[email]]</h3>
      <div class="row greybg">
          <div class="col-2 offset-1 redbg">Col 1</div>
          <div class="col-3 offset-2 greenbg">Col 2</div>
          <div class="col-4 bluebg">Col 3</div>
      </div>
      <button class="btn btn-primary btn-lg">Login</button>
      <button class="btn btn-secondary btn-sm">Logout</button>
      <button class="btn btn-success">Login</button>
      <button class="btn btn-warning">Logout</button>
      <button class="btn btn-info">Login</button>
      <button class="btn btn-danger">Logout</button>
      <button class="btn btn-light">Login</button>
      <button class="btn btn-dark">Logout</button>

      <iron-ajax
      auto
      id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String,
      }, last_name:{
        type: String,
      }, email: {
        type: String
      }, id:{
        type: Number
      }
    };
  } //end properties

showData(data){
  console.log("showData");
  console.log(data.detail.response);
  this.first_name=data.detail.response.first_name;
  this.last_name=data.detail.response.last_name;
  this.email=data.detail.response.email;
}

}//End class

window.customElements.define('visor-usuario', VisorUsuario);
