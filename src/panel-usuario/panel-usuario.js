import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../login-usuario2/login-usuario2.js'
import '../visor-usuario2/visor-usuario2.js'


/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1>Yo soy el padre</h1>
      <login-usuario2 on-loginsucess="processEvent"></login-usuario2>
      <visor-usuario2 id="receiver"></visor-usuario2>

      <iron-ajax
      id="doLogin"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>


    `;
  }

  static get properties() {
    return {
        id : {
          type: Number
      }
    };
  } //end properties

processEvent(e) {
  console.log("Capturado evento del emisor login-usuario2");
  console.log(e.detail);

  this.$.receiver.id = this.id;

  console.log("Se va a lanzar peticion getUserID");
  this.$.doLogin.generateRequest();
  console.log("Petición lanzada");


  //this.$.receiver.first_name = e.detail.name;
  //this.$.receiver.email = e.detail.email;


}

showError(error){
    console.log("Hubo un error");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse");
  //console.log(data.detail.response);
  //this.isLogged = true;
}

}//End class

window.customElements.define('panel-usuario', PanelUsuario);
