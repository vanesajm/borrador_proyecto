import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <button on-click="GetAccounts">Get Accounts</button>
      <input type="text" placeholder="id" value="{{id::input}}"></input>
      <h2>Sus cuentas son </h2>

      <dom-repeat items="{{accounts}}">
       <template>
         <h3>IBAN - {{item.iban}}</h3>
         <h3>Saldo - {{item.balance}}</h3>
       </template>
      </dom-repeat>


      <iron-ajax
      id="getAccounts"
      url="http://localhost:3000/apitechu/v2/accounts/{{id}}"
      handle-as="json"
      method="GET"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      iban: {
        type: String,
      }, balance:{
        type: Number,
      },
      id:{
        type: Number,
      },
      accounts:{
        type: Array
      }
    };
  } //end properties


GetAccounts() {

  this.$.getAccounts.generateRequest();

  console.log("enviada peticion");

  //this.iban=data.detail.response.iban;
  //this.balance=data.detail.response.balance;
}

showError(error){
    console.log("Hubo un error");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse");
  console.log(data.detail.response);
  this.accounts = data.detail.response;
  //this.iban = data.detail.response[0].iban;
  //this.balance = data.detail.response[0].balance;
}

}//End class

window.customElements.define('visor-cuenta', VisorCuenta);
