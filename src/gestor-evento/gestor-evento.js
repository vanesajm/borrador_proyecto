import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../emisor-evento/emisor-evento.js'
import '../receptor-evento/receptor-evento.js'

/**
 * @customElement
 * @polymer
 */
class GestorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1>Yo soy el padre</h1>
      <emisor-evento on-myevent="processEvent"></emisor-evento>
      <receptor-evento id="receiver"></receptor-evento>
    `;
  }

  static get properties() {
    return {
    };
  } //end properties

processEvent(e) {
  console.log("Capturado evento del emisor");
  console.log(e.detail);
  this.$.receiver.course = e.detail.course;
  this.$.receiver.year = e.detail.year;


}



}//End class

window.customElements.define('gestor-evento', GestorEvento);
