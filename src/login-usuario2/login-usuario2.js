import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class LoginUsuario2 extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Login Usuario</h3>
      <input type="text" placeholder="email" value="{{email::input}}"></input>
      <input type="password" placeholder="password" value="{{password::input}}"></input>
      <button on-click="login">Login</button>

      <span hidden$="{{!isLogged}}">Bienvenido/a de nuevo</span>

      <iron-ajax
      id="doLogin"
      url="http://localhost:3000/apitechu/v2/login"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {

      email: {
        type: String
      },
      password:{
        type: String
      },
      id:{
        type: Number,
      },
      isLogged:{
        type: Boolean,
        value:false
      }
    };
  } //end properties

login() {
  console.log("El usuario ha pulsado el boton");
  console.log("Voy a enviar la peticion");

  var loginData = {
    "email" : this.email,
    "pass" : this.password
  }

  console.log(loginData);
  this.$.doLogin.body = JSON.stringify(loginData);
  this.$.doLogin.generateRequest();

  console.log("Peticion enviada");
}

showError(error){
    console.log("Hubo un error");
    console.log(error);
}

manageAJAXResponse(data){
  console.log("manageAJAXResponse");
  console.log(data.detail.response);
  if (data.detail.response.msg == "Usuario con logged"){
    this.isLogged = true;
    console.log("LoginOK");
  }
  else {
    this.isLogged = false;
  }
  if (this.isLogged) {
     console.log("Lanzamos evento");
     console.log(data.detail.response.id);

     this.dispatchEvent(
       new CustomEvent(
         "myevent",{
           "detail" : {
             "id" : data.detail.response.id
           }
         }
       )
     )
  }

}

}//End class

window.customElements.define('login-usuario2', LoginUsuario2);
